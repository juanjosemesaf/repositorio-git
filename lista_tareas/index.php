<?php
include 'conexion.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>proyecto inicial(crud)</title>
    <link rel="stylesheet" href="styles.css">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous"> -->
</head>
<body>
    <button><a class="boton" href="formulario.php">Agregar tarea</a></button>
    <br>
    <br>
    <?php   
        $sql="SELECT *FROM tareas";
        $result=mysqli_query($conexion,$sql);        
        ?>
        
        <div class="container">
            <?php
            $pendientes = '';
            $completadas = '';
            $aplazadas = '';
            $rechazadas = '';

            while($mostrar=mysqli_fetch_array($result)){
                switch($mostrar['estado'] ){ 
                    case '1':
                    $pendientes = $pendientes. '<div class="tabla__tarea">
                    <h3> '.$mostrar['tarea'].' </h3>
                    <p> '.$mostrar['descripcion'].' </p>
                    <a class="tabla__tarea--boton1" href="editar.php?id= '.$mostrar['id'].'"> Editar</a>
                    <a class="tabla__tarea--boton2" href="eliminar.php?id=  '.$mostrar['id'].'">Eliminar</a> 
                    </div>';
                    break;
                    case '2':
                        $completadas = $completadas. '<div class="tabla__tarea">
                    <h3> '.$mostrar['tarea'].' </h3>
                    <p> '.$mostrar['descripcion'].' </p>
                    <a class="tabla__tarea--boton1" href="editar.php?id= '.$mostrar['id'].'"> Editar</a>
                    <a class="tabla__tarea--boton2" href="eliminar.php?id=  '.$mostrar['id'].'">Eliminar</a> 
                    </div>';
                    break;
                    case '3':
                        $aplazadas = $aplazadas. '<div class="tabla__tarea">
                    <h3> '.$mostrar['tarea'].' </h3>
                    <p> '.$mostrar['descripcion'].' </p>
                    <a class="tabla__tarea--boton1" href="editar.php?id= '.$mostrar['id'].'"> Editar</a>
                    <a class="tabla__tarea--boton2" href="eliminar.php?id=  '.$mostrar['id'].'">Eliminar</a> 
                    </div>';
                    break;
                    default:$rechazadas = $rechazadas. '<div class="tabla__tarea">
                    <h3> '.$mostrar['tarea'].' </h3>
                    <p> '.$mostrar['descripcion'].' </p>
                    <a class="tabla__tarea--boton1" href="editar.php?id= '.$mostrar['id'].'"> Editar</a>
                    <a class="tabla__tarea--boton2" href="eliminar.php?id=  '.$mostrar['id'].'">Eliminar</a> 
                    </div>';
                    break;
                    


                }

            ?>
        
        <?php
        }
        ?>
            
                <div class="tabla">
                    <h3 class="tabla__titulo">Lista de tareas</h3>
                    <?php
                    echo $pendientes
                    ?>
                </div>

                <div class="tabla">
                    <h3 class="tabla__titulo">Lista de tareas completadas</h3>
                    <?php
                    echo $completadas
                    ?>
                </div>

                <div class="tabla">
                    <h3 class="tabla__titulo">Lista de tareas aplazadas</h3>
                    <?php
                    echo $aplazadas
                    ?>
                </div>

                <div class="tabla">
                    <h3 class="tabla__titulo">Lista de tareas rechazadas</h3>
                    <?php
                    echo $rechazadas
                    ?>
                </div>
        
            



        </div>
    

        
    
        


    
    